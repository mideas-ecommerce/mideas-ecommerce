package com.mideas.ecommerce.customer;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mideas.ecommerce.RepositoryConfiguration;
import com.mideas.ecommerce.model.customer.Customer;
import com.mideas.ecommerce.service.customer.ICustomerQueryService;
import com.mideas.ecommerce.util.GsonUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { RepositoryConfiguration.class })
public class ExceptionTest {
	Logger mLogger = LogManager.getLogger(ExceptionTest.class);
	
	@Autowired
	ICustomerQueryService customerQueryService;

	@Test
	public void eception() throws Exception {
		Customer customer = new Customer();
		customer.setEmail("minbui@gmail.com");
		customer.setBirthday(new Date());
		customer.setFacebookUrl("https://www.facebook.com/minbui1008");
		customer.setGender("male");
		customer.setIdentifyNumber("221419701");
		customer.setPhoneNumber("0348791067");
		customer.setPassword("blnmttth");
		customerQueryService.addOrUpdate(customer);
		mLogger.info(GsonUtils.getGson().toJson(customer));
	}

}
