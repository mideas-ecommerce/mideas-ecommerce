package com.mideas.ecommerce;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@ComponentScan({"com.mideas.ecommerce"})
@EntityScan("com.mideas.ecommerce.model")
@EnableTransactionManagement
public class RepositoryConfiguration {
}
