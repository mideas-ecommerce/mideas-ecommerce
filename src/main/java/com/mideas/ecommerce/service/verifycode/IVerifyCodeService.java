package com.mideas.ecommerce.service.verifycode;

public interface IVerifyCodeService {
	Boolean verifyCode(String customerId, String code);
}
