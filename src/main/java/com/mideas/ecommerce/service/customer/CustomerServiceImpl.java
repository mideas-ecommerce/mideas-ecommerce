package com.mideas.ecommerce.service.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mideas.ecommerce.model.customer.Customer;

@Service
public class CustomerServiceImpl implements ICustomerService {
	@Autowired
	ICustomerQueryService customerQueryService;

	@Override
	public Customer getByPhonenumberAndPassword(String phonenumber, String password) {
		Customer customer = customerQueryService.findByPhonenumbeAndPassword(phonenumber, password);
		return customer;
	}

	@Override
	public void changePassword(String customerId, String oldPassword, String newPassword) throws Exception {
		Customer customer = this.getByIdAndPasswordThrowEception(customerId, oldPassword);
		customer.setPassword(newPassword);
		customerQueryService.addOrUpdate(customer);
	}

	@Override
	public void resetPassword(String phonenumber, String newPassword) throws Exception {
		Customer customer = this.getByPhonenumberThrowEception(phonenumber);
		customer.setPassword(newPassword);
		customerQueryService.addOrUpdate(customer);
	}

	@Override
	public Customer getByPhonenumberAndPasswordThrowEception(String phonenumber, String password) throws Exception {
		Customer customer = this.getByPhonenumberAndPassword(phonenumber, password);
		if (customer == null) {
			this.throwNotExistEception();
		}
		return customer;
	}

	@Override
	public Customer add(Customer customer) throws Exception {
		this.checkExistedCustomerByPhoneNumber(customer.getPhoneNumber());
		return customerQueryService.addOrUpdate(customer);
	}
	
	private Customer getByPhonenumberThrowEception(String phonenumber) throws Exception {
		Customer customer = customerQueryService.findByPhonenumber(phonenumber);
		if (customer == null) {
			this.throwNotExistEception();
		}
		return customer;
	}
	
	private Customer getByIdAndPasswordThrowEception(String customerId, String password) throws Exception {
		Customer customer = customerQueryService.findByIdAndPassword(customerId, password);
		if (customer == null) {
			this.throwNotExistOldPasswordEception();
		}
		return customer;
	}

	private void checkExistedCustomerByPhoneNumber(String phoneNumber) throws Exception {
		Customer existedCustomer = customerQueryService.findByPhonenumber(phoneNumber);
		if (existedCustomer != null) {
			this.throwExistPhoneNumberEception();
		}
	}
	
	private void throwNotExistOldPasswordEception() throws Exception {
		throw new Exception("Mật khẩu cũ không đúng, vui lòng kiểm tra lại");
	}

	private void throwNotExistEception() throws Exception {
		throw new Exception("Số điện thoại hoặc mật khẩu không đúng");
	}

	private void throwExistPhoneNumberEception() throws Exception {
		throw new Exception("Số điện thoại đã tồn tại trong hệ thống, vui lòng kiểm tra lại");
	}

}
