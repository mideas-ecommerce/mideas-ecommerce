package com.mideas.ecommerce.service.customer;

import com.mideas.ecommerce.model.customer.Customer;

public interface ICustomerService {
	Customer add(Customer customer) throws Exception;
	
	Customer getByPhonenumberAndPassword(String phonenumber, String password);
	
	public Customer getByPhonenumberAndPasswordThrowEception(String phonenumber, String password) throws Exception; 
	
	void changePassword(String customerId, String oldPassword, String newPassword) throws Exception;
	
	void resetPassword(String phonenumber, String newPassword) throws Exception;
}
