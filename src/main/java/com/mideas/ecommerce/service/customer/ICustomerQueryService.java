package com.mideas.ecommerce.service.customer;

import com.mideas.ecommerce.model.customer.Customer;

public interface ICustomerQueryService {
	Customer addOrUpdate(Customer customer);
	
	Customer findByIdAndPassword(String id, String password);
	
	Customer findByPhonenumber(String phonenumber);
	
	Customer findByPhonenumbeAndPassword(String phonenumber, String password);
}
