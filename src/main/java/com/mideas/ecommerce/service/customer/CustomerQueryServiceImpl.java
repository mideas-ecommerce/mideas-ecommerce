package com.mideas.ecommerce.service.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mideas.ecommerce.model.customer.Customer;
import com.mideas.ecommerce.repository.customer.ICustomerRepository;

@Service
public class CustomerQueryServiceImpl implements ICustomerQueryService{
	
	@Autowired
	ICustomerRepository customerRepo;

	@Override
	public Customer addOrUpdate(Customer customer) {
		return customerRepo.save(customer);
	}

	@Override
	public Customer findByPhonenumber(String phonenumber) {
		return customerRepo.findByPhoneNumber(phonenumber);
	}

	@Override
	public Customer findByPhonenumbeAndPassword(String phonenumber, String password) {
		return customerRepo.findByPhoneNumberAndPassword(phonenumber, password);
	}

	@Override
	public Customer findByIdAndPassword(String id, String password) {
		return customerRepo.findByIdAndPassword(id, password);
	}

}
