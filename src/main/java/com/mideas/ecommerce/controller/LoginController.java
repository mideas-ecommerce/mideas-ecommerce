
package com.mideas.ecommerce.controller;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mideas.ecommerce.model.common.DefaultRespond;
import com.mideas.ecommerce.model.customer.Customer;
import com.mideas.ecommerce.service.customer.ICustomerService;
import com.mideas.ecommerce.service.verifycode.IVerifyCodeService;

@RestController
@RequestMapping("${server.customer.contextPath}")
public class LoginController {
	Logger mLogger = LogManager.getLogger(LoginController.class);
	
	@Autowired
	ICustomerService customerService;
	
	@Autowired
	IVerifyCodeService verifyCodeService;
	
	@PostMapping("/login")
	public Customer login(@RequestBody HashMap<String, String> body) throws Exception {
		return customerService.getByPhonenumberAndPasswordThrowEception(body.get("phonenumber"), body.get("password"));
	}
	
	@PutMapping("/login/password")
	public DefaultRespond resetPassword(@RequestBody HashMap<String, String> body) throws Exception {
		customerService.resetPassword(body.get("phonenumber"), body.get("newPasswod"));
		return new DefaultRespond(true);
	}
	
	@PutMapping("/profile/password")
	public DefaultRespond changePassword(@RequestBody HashMap<String, String> body, String customerId) throws Exception {
		customerService.changePassword(customerId, body.get("oldPassword"), body.get("newPasswod"));
		return new DefaultRespond(true);
	}
	
	@GetMapping("/code/get")
	public DefaultRespond getVerifyCode() throws Exception {
		return new DefaultRespond(true);
	}
	
	@PostMapping("/code/verify")
	public DefaultRespond verifyCode(@RequestBody HashMap<String, String> body) throws Exception {
		Boolean isSuccess = verifyCodeService.verifyCode("", body.get("code"));
		return new DefaultRespond(isSuccess);
	}
}
