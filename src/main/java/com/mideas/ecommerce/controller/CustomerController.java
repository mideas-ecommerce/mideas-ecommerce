
package com.mideas.ecommerce.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mideas.ecommerce.model.customer.Customer;
import com.mideas.ecommerce.service.customer.ICustomerService;

@RestController
@RequestMapping("${server.customer.contextPath}")
public class CustomerController {
	Logger mLogger = LogManager.getLogger(CustomerController.class);
	
	@Autowired
	ICustomerService customerService;
	
	@PostMapping("/customer")
	public Customer login(@RequestBody Customer customer) throws Exception {
		return customerService.add(customer);
	}
	
	
}
