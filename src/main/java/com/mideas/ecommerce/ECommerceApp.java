package com.mideas.ecommerce;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.mideas.ecommerce" })
public class ECommerceApp implements CommandLineRunner {
	Logger logger = LogManager.getLogger(ECommerceApp.class);

	public void run(String... args) throws Exception {
		logger.info("--------- APP START SUCESS -----------");
	}

//	@Bean
//	public LocalValidatorFactoryBean validator(MessageSource messageSource) {
//		LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
//		validatorFactoryBean.setValidationMessageSource(messageSource);
//		return validatorFactoryBean;
//	}

	public static void main(String[] args) {
		SpringApplication.run(ECommerceApp.class, args);
	}
}
