package com.mideas.ecommerce.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mideas.ecommerce.model.customer.Customer;
@Repository
public interface ICustomerRepository extends JpaRepository<Customer, String>{
	Customer findByPhoneNumber(String phoneNumber);
	
	Customer findByPhoneNumberAndPassword(String phoneNumber, String password);
	
	Customer findByIdAndPassword(String id, String password);
}
