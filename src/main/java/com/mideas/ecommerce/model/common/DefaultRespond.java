package com.mideas.ecommerce.model.common;

import java.util.HashMap;

public class DefaultRespond extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	public DefaultRespond(boolean isSuccess) {
		this.setSuccess(isSuccess);
	}

	public boolean isSuccess() {
		return (boolean) this.get("isSuccess");
	}

	public void setSuccess(boolean isSuccess) {
		this.put("isSuccess", isSuccess);
	}

	public String message() {
		return (String) this.get("message");
	}

	public void setMessage(boolean message) {
		this.put("message", message);
	}
}