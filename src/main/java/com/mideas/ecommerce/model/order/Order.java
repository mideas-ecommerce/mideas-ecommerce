package com.mideas.ecommerce.model.order;

public class Order extends BaseOrder{
	OrderDeliveryDate estimatedDeliveryDate;
	String paymentMode;
	String address;
	String phoneNumber;
	OrderCost cost;
}
