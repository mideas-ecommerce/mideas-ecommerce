package com.mideas.ecommerce.model.product;

import java.util.List;

public class Product extends BaseProduct{
	String productId;
	List<String> categoryId;
	ProductPrice productPrice;
	int quantityOfInventory;
	String productUnit;	
	String brand;
	String originOfBrand;
	String vendor;
	String ingredient;
}
