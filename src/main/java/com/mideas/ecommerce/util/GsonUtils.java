package com.mideas.ecommerce.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtils {
	private static final GsonUtils INSTANCE = new GsonUtils();

	private Gson gson;

	public GsonUtils() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gson = gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
	}

	public static Gson getGson() {
		return INSTANCE.gson;
	}

	public static Gson getGsonDisableHTMLEncaping() {
		return new GsonBuilder().disableHtmlEscaping().create();
	}
}
